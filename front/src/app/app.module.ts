import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './components/form/form.component';
import { ArticleComponent } from './components/article/article.component';
import { FormPageComponent } from './pages/form-page/form-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { DetailsPageComponent } from './pages/details-page/details-page.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { NavigationComponent } from './components/layout/navigation/navigation.component';
import { SearchComponent } from './components/search/search.component';
import { LucideAngularModule, Plus, Trash2, Search, Home, Edit} from 'lucide-angular';
import { FormsModule } from '@angular/forms';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LoginComponent } from './components/login/login.component';
import { FormUpdateComponent } from './components/form-update/form-update.component';
import { UpdatePageComponent } from './pages/update-page/update-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';
import { SignupComponent } from './components/signup/signup.component';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ArticleComponent,
    FormPageComponent,
    HomePageComponent,
    DetailsPageComponent,
    HeaderComponent,
    NavigationComponent,
    SearchComponent,
    LoginPageComponent,
    LoginComponent,
    FormUpdateComponent,
    UpdatePageComponent,
    SignupPageComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LucideAngularModule.pick({Plus, Trash2, Search, Home, Edit})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
