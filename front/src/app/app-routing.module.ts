import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsPageComponent } from './pages/details-page/details-page.component';
import { FormPageComponent } from './pages/form-page/form-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { UpdatePageComponent } from './pages/update-page/update-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'articles', pathMatch: 'full'},
  { path: 'articles', component: HomePageComponent},
  { path: 'articles/form', component: FormPageComponent},
  { path: 'articles/form/:articleid', component: UpdatePageComponent},
  { path: 'articles/login', component: LoginPageComponent}, 
  { path: 'articles/signup', component: SignupPageComponent},  
  { path: 'articles/:articleid', component: DetailsPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
