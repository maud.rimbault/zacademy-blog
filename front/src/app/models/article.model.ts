export interface Article {
    articleid: string;
    title: string;
    summary: string;
    content: string;
}