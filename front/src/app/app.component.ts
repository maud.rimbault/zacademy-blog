import { Component, OnInit } from '@angular/core';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'front';

  constructor(public authService: AuthService, private router:Router) {

    axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        if (config.headers && config.method !== 'get')
          config.headers['Authorization'] = authService.getCurrentUserBasicAuthentication();
        return config
      },
      (error: AxiosError) => {
        console.error('ERROR:', error)
        Promise.reject(error)
      })


    axios.interceptors.response.use(response => {
      return response;
    }, error => {
      if (error.response.status === 401) {
            this.router.navigate(['/articles/login']);
      }
      throw error;
    });

  }

  ngOnInit() {
    if (sessionStorage.getItem('isLogged') === 'true') {
      this.authService.isLogged = true;
    }
  }
}
