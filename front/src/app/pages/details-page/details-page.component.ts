import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {

  private articleid?: string;
  @Input()
  public article?: Article;

  @Output('articleDetails')
  public articleDetailEvent = new EventEmitter<Article>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleService,
    private router: Router
  ) {

    // this.articleid = parseInt(this.activatedRoute.snapshot.params['id'], 10);
    this.articleid = this.activatedRoute.snapshot.params['articleid'];
    //console.log(this.activatedRoute.snapshot);
    //console.log(this.articleid);
    const article = this.articleService.articles.find((a) => a.articleid === this.articleid);
    
    //const article = this.articleService.getArticle(this.articleid);
    if (!article) {
     return;
     ;
    }

    this.article = article;
  }


  

  async deleteArticle(articleid: string){
    console.log("articleid");
    console.log(articleid);
    this.articleService.deleteArticle(articleid);
    await this.router.navigate(['/']);
    await this.articleService.fetchArticles();
    console.log(articleid);
  }

  
  ngOnInit(): void {
    // this.activatedRoute.params.subscribe(params => {
    //   this.articleService.getArticle(params['articleid']);
    // })

  }

}


