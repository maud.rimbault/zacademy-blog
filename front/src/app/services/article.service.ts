import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../models/article.model';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  public articles: Article[] = [];

  public async fetchArticles() {
    await this.getListArticles();
  }


  public async getListArticles() {    
     await axios.get("http://localhost:8080/api/articles/")
      .then((res) => {
        //console.log(res.data);
        this.articles = res.data;
      })
      .catch((err) => {
        console.log(err);
      });
  }

  //Solution d'Eric qui fonctionne egalement
// public async getListArticles(): Promise<void> {
//     const { data } = await axios.get("http://localhost:8080/api/articles/");
//     this.articles = data.map((article: any) => {
//       console.log(article)
//       return {
//         articleid: article.articleid,
//         title: article.title,
//         summary: article.summary,
//         content: article.content,
//       };
//     });
//   }

public getSearchListArticles(wordTitle: string) {
  console.log(`${wordTitle}`);
 axios.get(`http://localhost:8080/api/articles?wordTitle=${wordTitle}`)
     .then((res) => {
      console.log(res.data);
       this.articles = res.data;
     })
     .catch((err) => {
       console.log(err);
     });
 }


  public async deleteArticle(articleid: string) {
    console.log(articleid)
    axios.delete("http://localhost:8080/api/articles/" + articleid)
    //axios.delete("http://localhost:8080/api/articles/" + articleid, 
    //{headers:{'Authorization': 'Basic dG90bzp0b3Rv'}})
    .then((res) => {
      this.fetchArticles();
    })
  }

  async getArticle(articleid: string): Promise<Article> {
    const {data} = await axios.get("http://localhost:8080/api/articles/" + articleid);
    //console.log(articleid); 
    console.log(data);
    return data;
    //console.log(response);
    // return {
    //     //title: response.
    //   articleid: response.data.articleid,
    //   title: response.data.title,
    //   summary: response.data.summary,
    //   content: response.data.content
    //   //this.articles = res.data;
    // }
}

public signup(userData: User) {
  const apiUrl = "http://localhost:8080/api/users/";
  return axios.post(`${apiUrl}`, userData)
}

  public createArticle(articleData: Article) {
    const apiUrl = "http://localhost:8080/api/articles";
   console.log(this.authService.getUsername());
    return axios.post(`${apiUrl}`, articleData, { params: { usernameAuthor: this.authService.getUsername()}}
      ) // toto:toto
    //axios.post(`${apiUrl}`, articleData, 
    //{headers:{'Authorization': 'Basic dG90bzp0b3Rv'}}) // toto:toto
      
  }

  public editArticle(articleid: String, title: String, summary: String, content: String) {
    console.log(articleid);
    axios.put("http://localhost:8080/api/articles/"+ articleid, {
      title: title,
      summary: summary,
      content: content
    })
      .then((res) => {
        this.fetchArticles();
      })
      .catch((err) => {
        console.log(err);
      })
  }




  // public createArticle(articleData: Article) {
  //   const apiUrl = "http://localhost:8080/api/articles/";
  //   axios.post(`${apiUrl}`, articleData)
  //     .then((res) => {
  //       console.log(res.data);
  //       this.getListArticles();
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }

  constructor(private authService: AuthService) { }
}
