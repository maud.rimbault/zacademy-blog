import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly SESSION_STORAGE_KEY = "currentUser";
  public isLogged: boolean = false;

  login(user: User) {
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
    sessionStorage.setItem('isLogged', 'true');
    this.isLogged = true;
  }

  getUsername(){
    const json = sessionStorage.getItem("currentUser");
    if (json) {
      return JSON.parse(json).username;
      //console.log(currentUsername);
    }
  }

  logout() {
    sessionStorage.clear();
    sessionStorage.setItem('isLogged', 'false');
    this.isLogged = false;
  }

  getCurrentUserBasicAuthentication(): string {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return "";
    }
  }

  constructor() { }
}
