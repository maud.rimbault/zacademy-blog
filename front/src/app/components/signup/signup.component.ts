import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  password: string = "";
  email: string = "";
  username: string = "";


  constructor(public articleService: ArticleService, private router: Router) { }

  async signupForm() {
    const userData:User= {
      username: this.username,
      email: this.email,
      password: this.password
    };

    await this.articleService.signup(userData);
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
  }

}
