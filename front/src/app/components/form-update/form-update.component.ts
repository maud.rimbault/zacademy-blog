import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-form-update',
  templateUrl: './form-update.component.html',
  styleUrls: ['./form-update.component.scss']
})

export class FormUpdateComponent implements OnInit {
  public article!: Article; 
  public articleid: string = '';
  public title: string = '';
  public summary: string = '';
  public content: string = '';

  constructor(private articleService: ArticleService, private router: Router, private activatedRoute: ActivatedRoute) { 

  }

  articleFormUpdate(): void {
    this.activatedRoute.params.subscribe(params => {
      this.articleService.editArticle(this.articleid, this.title, this.summary, this.content);
      this.router.navigate(["/"]);
    })
  }

  async ngOnInit() {
    this.articleid = this.activatedRoute.snapshot.params['articleid'];
    //console.log(this.activatedRoute.snapshot);
    //console.log(this.articleid);
    //this.articleService.getArticle(this.articleid);

    await this.articleService.getArticle(this.articleid).then((res) => {
    this.article = res;
    })

    //console.log(this.article);
    this.articleid = this.article.articleid;
    this.title = this.article.title;
    //console.log(this.title);
    this.summary = this.article.summary;
    this.content = this.article.content;

    //console.log(this.activatedRoute.snapshot);

    // this.activatedRoute.params.subscribe(params => {
    //   this.articleid = params['articleid'];
    //   this.articleService.getArticle(this.articleid);
    //   console.log(this.articleid);
    // })
  }
}
