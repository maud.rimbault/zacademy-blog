import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {


  constructor(public authService: AuthService, public article:ArticleService) {
   }

  logout(){
    this.authService.logout();
    //this.articleService.fetchArticles();
  }


  ngOnInit(): void {
  }

}


