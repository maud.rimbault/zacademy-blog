import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  articleid: string="";
  title: string="";
  summary: string="";
  content: string="";

  constructor(public articleService: ArticleService, private router: Router) { }

  async articleFormInfo() {
    const articleData:Article= {
      title: this.title,
      summary: this.summary,
      content: this.content,
      //articleid: Math.floor(Math.random() * (10000 + 1)),
      articleid: this.articleid
    };

     this.articleService.createArticle(articleData).then(() =>{
       // refresh articles
      this.articleService.getListArticles()
      // and go to home
      this.router.navigate(['/']);

     }
     )
  }

  ngOnInit(): void {
  }

}
