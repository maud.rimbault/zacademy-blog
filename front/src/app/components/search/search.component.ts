import { Component, OnInit } from '@angular/core';
import { ArticleService } from 'src/app/services/article.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  wordTitle: string="";

  constructor(private articleService: ArticleService) {

  }

  getSearchResult(wordTitle: any) {
    this.wordTitle = wordTitle.target.value;
    console.log("word a trouver : ", this.wordTitle);
    }

  async findWordinTitle(wordTitle: string) {
    await this.articleService.getSearchListArticles(wordTitle);
  };

}
