INSERT INTO users(userid, email, username, password)
VALUES('e3e65cb2-c0ae-11ec-9d64-0242ac120002', 'anonymous@anonymous', 'inconnu', '$2y$10$x00R9Tpx/K/onYPfj1WyVe.Ul5nzSi3wT7d.jzeDrA3mYSW67eeYa') ON CONFLICT DO NOTHING;

INSERT INTO users(userid, email, username, password)
VALUES('407144a3-1e81-4e0e-bc85-7e8302363f15', 'toto@toto.fr', 'toto', '$2y$10$lydQ96lj6GtuIfnhwn9.vuMKsB8iH/IX3erpUsHhJ1X22gjHEDS6e') ON CONFLICT DO NOTHING;

INSERT INTO articles(articleid, title, summary, content, status, userid)
VALUES('49e2508a-e16e-459a-ae36-2e7fdd4b4803', '1 Lorem ipsum dolor sit amet', '1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo, dui et placerat egestas, augue metus ultricies augue, at pharetra.', '1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consectetur felis, in facilisis velit. Sed id feugiat mi. Morbi sit amet dapibus enim, ac consectetur dolor. Praesent dolor orci, faucibus non pellentesque eu, mollis eget dolor. Nam vestibulum, mi ut lacinia accumsan, mi dolor sagittis urna, nec feugiat enim.', '0', '407144a3-1e81-4e0e-bc85-7e8302363f15') ON CONFLICT DO NOTHING;
INSERT INTO articles(articleid, title, summary, content, status, userid)
VALUES('8338ae87-a4cf-4678-b408-92f2ae549268', '2 Lorem ipsum dolor sit amet', '2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo, dui et placerat egestas, augue metus ultricies augue, at pharetra.', '1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consectetur felis, in facilisis velit. Sed id feugiat mi. Morbi sit amet dapibus enim, ac consectetur dolor. Praesent dolor orci, faucibus non pellentesque eu, mollis eget dolor. Nam vestibulum, mi ut lacinia accumsan, mi dolor sagittis urna, nec feugiat enim.', '0', '407144a3-1e81-4e0e-bc85-7e8302363f15') ON CONFLICT DO NOTHING;
