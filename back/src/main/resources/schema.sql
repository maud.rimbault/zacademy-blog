CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE IF NOT EXISTS users(
    userid char(36) primary key,
    email text not null unique,
    username text not null unique,
    password text not null
);

CREATE TABLE IF NOT EXISTS articles(
    articleid char(36) primary key,
    title text not null unique,
    summary text not null,
    content text not null,
    status text not null,
    userid char(36) references users(userid)
);