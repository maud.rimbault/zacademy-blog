package com.zenika.academy.barbajavas.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZacademyBlogApplication {
	public static void main(String[] args) {
		SpringApplication.run(ZacademyBlogApplication.class, args);
	}
}