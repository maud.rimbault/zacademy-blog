package com.zenika.academy.barbajavas.blog.web.users;

import com.zenika.academy.barbajavas.blog.application.UserService;
import com.zenika.academy.barbajavas.blog.domain.model.article.Article;
import com.zenika.academy.barbajavas.blog.domain.model.users.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    //authentification (plus besoin de createUSer?)
//    @PostMapping("/auth")
//    ResponseEntity<User> auth() {
//        SecurityContextHolder.getContext().getAuthentication().getName();
//        User user = new User(UUID.randomUUID().toString(),"anonymous@anonymous.com",SecurityContextHolder.getContext().getAuthentication().getName());
//        return ResponseEntity.status(HttpStatus.OK).body(user);
//    }

    @PostMapping
    ResponseEntity<User> createUser(@RequestBody UserDto body) {
        User createdUser = this.userService.createUser(body.email(), body.username(), body.password());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @GetMapping("/{userid}")
    ResponseEntity<User> findUser(@PathVariable String userid) {
        return userService.findByTid(userid).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/{userid}/articles")
    List<Article> findUserArticles(@PathVariable String userid) {
        return userService.listArticle(userid);
    }

    @GetMapping
    List<User> searchUsers(@RequestParam("email") String email) {
        return userService.findByEmail(email)
                .map(List::of)
                .orElseGet(Collections::emptyList);
    }
}
