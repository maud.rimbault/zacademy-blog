package com.zenika.academy.barbajavas.blog.web.users;

public record UserDto(String email, String username, String password) {
}

