package com.zenika.academy.barbajavas.blog.web.articles;

import com.zenika.academy.barbajavas.blog.application.ArticleService;
import com.zenika.academy.barbajavas.blog.application.UserService;
import com.zenika.academy.barbajavas.blog.domain.model.article.Article;
import com.zenika.academy.barbajavas.blog.domain.model.users.User;
import com.zenika.academy.barbajavas.blog.domain.model.users.UserNotFoundException;
import com.zenika.academy.barbajavas.blog.domain.repository.UserRepository;
import com.zenika.academy.barbajavas.blog.web.users.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
//@RequestMapping("/api/articles") ! qd ajout sécurité, pb ne prend pas "/api" de application.yml
//@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;
    private final UserService userService;

    @Autowired
    public ArticleController(ArticleService articleService, UserService userService) {
        this.articleService = articleService;
        this.userService = userService;
    }

    //Creation Article
    @PostMapping
    ResponseEntity<Article> createArticle(@RequestBody ArticleDto body, @RequestParam String usernameAuthor) throws UserNotFoundException {
        //Optional<User> author =  this.userService.findIdByUsernameAuthor(usernameAuthor);
        Article createdArticle = articleService.createArticle(body.title(), body.summary(), body.content(), usernameAuthor);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
    }

//    //Creation Article - avant @RequestParam
//    @PostMapping
//    ResponseEntity<Article> createArticle(@RequestBody ArticleDto body) throws UserNotFoundException {
//        Article createdArticle = articleService.createArticle(body.title(), body.summary(), body.content(), body.userid());
//        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
//    }

    //Solution possible Amandine
//    @PostMapping
//    @ResponseStatus(value = HttpStatus.CREATED)
//    public Article createArticle(@RequestBody ArticleDto body) throws BadLengthSummaryException {
//        SecurityContextHolder.getContext().getAuthentication().getName();
//        return this.articleService.createArticle(body.title(), body.summary(), body.content());
//    }


    // #record-dto mais pas ResponseEntity
    //public Article createArticle(@RequestBody ArticleDto body) {
    //    return this.articleService.createArticle(body.title(), body.summary(), body.content());

    //// #version sans record-dto
    ////public Article createArticle(@RequestBody ArticleDto articleDto) {
    ////    return this.articleService.createArticle(articleDto.getTitle(), articleDto.getSummary(), articleDto.getContent());
    //}

//    //Afficher tous les articles
//    @GetMapping
//    Iterable<Article> listArticles() {
//        return this.articleService.getAllArticles();
//    }

    //Afficher tous les articles ou findByTitle
    @GetMapping
    ResponseEntity<Iterable<Article>> listArticles(@RequestParam(required = false) String wordTitle) {
       if (wordTitle != null){
           return new ResponseEntity<>(this.articleService.getArticlesFindByTitle(wordTitle), HttpStatus.OK);
       }
        return new ResponseEntity<>(this.articleService.getAllArticles(), HttpStatus.OK);
    }


    //Afficher un article
    @GetMapping("/{articleid}")
    public Optional<Article>
    findArticleById(@PathVariable("articleid") String articleid){
        return this.articleService.getArticle(articleid);
    }

    //Delete un article
    @DeleteMapping("/{articleid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteArticle(@PathVariable String articleid) {
        this.articleService.deleteArticle(articleid);
    }

    //Update un article
    @PutMapping("/{articleid}")
    ResponseEntity<Void> updateArticle(@PathVariable String articleid, @RequestBody ArticleDto body) {
        articleService.changeArticle(articleid, body.title(), body.summary(), body.content());
        return ResponseEntity.ok().build();
    }
}
