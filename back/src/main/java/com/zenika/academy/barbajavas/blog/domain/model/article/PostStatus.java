package com.zenika.academy.barbajavas.blog.domain.model.article;

public enum PostStatus {
    DRAFT,
    PUBLISHED
}
