package com.zenika.academy.barbajavas.blog.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Collections;


//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//import org.springframework.security.web.SecurityFilterChain;
import java.util.Collections;
//import java.util.List;

@Configuration
public class securityConfiguration {
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .antMatcher("/**")
                .csrf().disable() // CSRF protection is enabled by default in the Java configuration.
                //.authorizeRequests(authorize -> authorize
                //.authorizeRequests().antMatchers(HttpMethod.GET, "/api/articles").permitAll()
                //.anyRequest().authenticated()
                //.and()
                .authorizeRequests(authorize -> authorize
                        .antMatchers(HttpMethod.GET, "/articles").permitAll()
                        .antMatchers(HttpMethod.GET, "/articles/*").permitAll() //autorise 2 routes GET
                        .antMatchers(HttpMethod.GET, "/users/*").permitAll()
                        .antMatchers(HttpMethod.POST, "/users").permitAll()
                        .anyRequest().authenticated())
                // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .build();
    }

    //Amandine
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("HEAD",
                        "GET", "POST", "PUT", "DELETE", "PATCH");
            }
        };
    }

//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurer() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("HEAD",
//                        "GET", "POST", "PUT", "DELETE", "PATCH");
//            }
//        };
//    }

 //   @Bean
//    UserDetailsService userDetailsService(UserRepository userRepository) {
//        return new UserDetailsService() {
//            @Override
//            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
////                    Iterable<User> test = userRepository.findAll();
////                for (User user: test) {
////                    System.out.println(user);
////                }
//                return userRepository.findByUsername(username).orElse(null);
//            }
//        };
//    }

    //encoder le password à l'inscription
//    @Bean
//    public PasswordEncoder encoder() {
//        return new BCryptPasswordEncoder();
//    }



    @Bean
    public UserDetailsService users(){
        UserDetails maxime = User.builder()
                .username("maxime")
                .password("{noop}maxime")
                .roles("USER")
                .build();
        UserDetails amandine = User.builder()
                .username("amandine")
                .password("{noop}password")
                .roles("USER")
                .build();
        UserDetails maud = User.builder()
                .username("maud")
                .password("{bcrypt}$2a$10$wtlj0RNm1rFml/ryg9/D5eWIw9MIz908kxbTzFiq.ZtsPNKfXgDe2")
                .roles("MANAGER")
                .build();
        UserDetails aymeric = User.builder()
                .username("aymeric")
                .password("{MD5}e9ed1105d655d45535d3b3235dc32d60")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(maxime, amandine, maud, aymeric);
    }
}
