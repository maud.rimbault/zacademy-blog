package com.zenika.academy.barbajavas.blog.application;

import com.zenika.academy.barbajavas.blog.domain.model.article.Article;
import com.zenika.academy.barbajavas.blog.domain.model.users.User;
import com.zenika.academy.barbajavas.blog.domain.repository.ArticleRepository;
import com.zenika.academy.barbajavas.blog.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final ArticleRepository articleRepository;
    //private PasswordEncoder passwordEncoder;

    @Autowired
    //public UserService(UserRepository userRepository, ArticleRepository articleRepository, PasswordEncoder passwordEncoder) {
    public UserService(UserRepository userRepository, ArticleRepository articleRepository) {
        this.userRepository = userRepository;
        this.articleRepository = articleRepository;
        //this.passwordEncoder = passwordEncoder;
    }

    public User createUser(String email, String username, String password) {
        User newUser = new User(UUID.randomUUID().toString(), email, username, password);
        //newUser.setPassword(passwordEncoder.encode(password));
        userRepository.save(newUser);
        return newUser;
    }

    public Optional<User> findByTid(String userid) {
        return userRepository.findById(userid);
        //return userRepository.findByTid(userTid);
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public Optional<User> findIdByUsernameAuthor(String username) {
        return userRepository.findByUsername(username);
    }

    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }
    public List<Article> listArticle(String userid) {
        if(userid != null && !userRepository.existsById(userid)) {
            throw new IllegalArgumentException("User does not exist");
        }
        return Collections.emptyList();
        //return articleRepository.findByUser(userRepository.findById(userid).get());
    }
}
