package com.zenika.academy.barbajavas.blog.application;

import com.zenika.academy.barbajavas.blog.domain.model.article.Article;
import com.zenika.academy.barbajavas.blog.domain.model.article.PostStatus;
import com.zenika.academy.barbajavas.blog.domain.model.users.User;
import com.zenika.academy.barbajavas.blog.domain.model.users.UserNotFoundException;
import com.zenika.academy.barbajavas.blog.domain.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

//annotation service plutôt que component : un service est un component
@Service
public class ArticleService {
    private final ArticleRepository articleRepository;
    private final UserService userService;

    @Autowired
    public ArticleService(ArticleRepository articleRepository, UserService userService) {
        this.articleRepository = articleRepository;
        this.userService = userService;
    }

    //public Article createArticle(String title, String summary, String content, String userid) {
    public Article createArticle(String title, String summary, String content, String usernameAuthor) throws UserNotFoundException {
        User user = usernameAuthor != null
                ? userService.findIdByUsernameAuthor(usernameAuthor).orElseThrow(UserNotFoundException::new)
                : null;

        //User user = usernameAuthor != null
                //? userService.findIdByUsernameAuthor(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new)
                //: null;
         Article newArticle = new Article(UUID.randomUUID().toString(), title, summary, content, PostStatus.DRAFT, user);
        articleRepository.save(newArticle);
        return newArticle;
    }

    public Iterable<Article> getAllArticles(){
        return articleRepository.findAll();
    }

    public Optional<Article> getArticle(String articleid) {
        return articleRepository.findById(articleid);
    }

    public Iterable<Article> getArticlesFindByTitle(String wordTitle){
        return articleRepository.findAllByTitleContaining(wordTitle);
    }

    public void deleteArticle(String articleid) {
        this.articleRepository.deleteById(articleid);
    }

    public void changeArticle(String articleid, String newTitle, String newSummary, String newContent) {
        Article article = this.articleRepository.findById(articleid).get();
        article.setTitle(newTitle);
        article.setSummary(newSummary);
        article.setContent(newContent);

        this.articleRepository.save(article);
    }
}
