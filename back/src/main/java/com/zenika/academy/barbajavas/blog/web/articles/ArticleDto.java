package com.zenika.academy.barbajavas.blog.web.articles;

public record ArticleDto(String title, String summary, String content, String userid) {
}

// #version sans record
//public class ArticleDto {
//    private String title;
//    private String summary;
//    private String content;
//
//    public String getTitle() {
//        return title;
//    }
//
//    public String getSummary() {
//        return summary;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public void setSummary(String summary) {
//        this.summary = summary;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//}