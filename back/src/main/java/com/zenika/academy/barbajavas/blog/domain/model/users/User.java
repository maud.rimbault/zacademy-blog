package com.zenika.academy.barbajavas.blog.domain.model.users;

import com.zenika.academy.barbajavas.blog.domain.model.article.Article;
import lombok.Builder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


@Entity
@Table(name = "users")
@Access(AccessType.FIELD)
public class User implements UserDetails {
    @Id
    private String userid;
    private String email;
    private String username;
    private String password;


   // @OneToMany(fetch = FetchType.EAGER, mappedBy = "userid")
    //private List<Article> articles;


    protected User() {
        //For JPA
    }

    public User(String userid, String email, String username, String password) {
        this.userid = userid;
        this.email = email;
        this.username = username;
        this.password = password;
        //this.articles = new ArrayList<>();
    }


    public void setPassword(String password) {
        this.password = password;
    }




    public String getUserId() {
        return userid;
    }

//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return Collections.EMPTY_LIST;
//    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getEmail() {
        return email;
    }
}
