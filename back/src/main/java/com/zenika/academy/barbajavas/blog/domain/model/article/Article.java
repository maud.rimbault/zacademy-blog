package com.zenika.academy.barbajavas.blog.domain.model.article;

import com.zenika.academy.barbajavas.blog.domain.model.users.User;
import lombok.Builder;

import javax.persistence.*;


@Entity
@Table(name = "articles")
@Access(AccessType.FIELD)
public class Article {
    @Id
    //@Column(name="articleid")
    private String articleid;
    private String title;
    private String summary;
    private String content;
    @Column(name="status")
    private PostStatus status_post;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userid")
    private User user;


    protected Article() {
        // for JPA
    }


    public Article(String articleid, String title, String summary, String content, PostStatus status_post, User user) {
        this.articleid = articleid;
        this.title = title;
        this.summary = summary;
        this.content = content;
        this.status_post = status_post;
        this.user = user;
    }

    public String getArticleid() {
        return articleid;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public String getContent() {
        return content;
    }

    public PostStatus getStatus_post() {
        return status_post;
    }

    public User getUser() {
        return user;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
