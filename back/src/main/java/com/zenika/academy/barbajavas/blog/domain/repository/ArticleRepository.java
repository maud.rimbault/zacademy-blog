package com.zenika.academy.barbajavas.blog.domain.repository;

import com.zenika.academy.barbajavas.blog.domain.model.article.Article;
import com.zenika.academy.barbajavas.blog.domain.model.users.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {
   //List<Article> findByUser(User user);
   @Query(value = "select * from articles where word_similarity(?1, articles.title) > 0.3", nativeQuery = true)
   Iterable<Article> findAllByTitleContaining(@Param("title") String title);
}
